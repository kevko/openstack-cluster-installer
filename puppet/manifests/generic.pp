class oci::generic(
#  $machine_hostname         = undef,
#  $machine_ip               = undef,
  $etc_hosts                = undef,
  $time_server_host         = undef,
#  $time_server_use_pool     = false,
){
  ::oci::sysctl { 'oci-rox': }

  class { '::oci::etchosts':
    etc_hosts_file => $etc_hosts,
  }

  class {
    '::oci::chrony': time_server_host => $time_server_host,
  }

  if $facts['os']['lsb'] != undef{
    $mycodename = $facts['os']['lsb']['distcodename']
  }else{
    $mycodename = $facts['os']['distro']['codename']
  }

  if $mycodename != 'stretch'{
    alternatives { 'iptables':
      path => '/usr/sbin/iptables-legacy',
    }
    alternatives { 'ip6tables':
      path => '/usr/sbin/ip6tables-legacy',
    }
  }
}
