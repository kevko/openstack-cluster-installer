class oci::network(
  $openstack_release        = undef,
  $cluster_name             = undef,
  $machine_hostname         = undef,
  $machine_ip               = undef,
  $bridge_mapping_list      = undef,
  $external_network_list    = undef,
  $first_master             = undef,
  $first_master_ip          = undef,
  $cluster_domain           = undef,
  $vmnet_ip                 = undef,
  $vmnet_mtu                = undef,
  $all_masters              = undef,
  $all_masters_ip           = undef,
  $vip_hostname             = undef,
  $vip_ipaddr               = undef,
  $vip_netmask              = undef,
  $self_signed_api_cert     = true,
  $sql_vip_ip               = undef,
  $network_ipaddr           = undef,
  $network_cidr             = undef,
  $use_ssl                  = true,
  $pass_nova_authtoken      = undef,
  $pass_neutron_authtoken   = undef,
  $pass_neutron_messaging   = undef,
  $pass_metadata_proxy_shared_secret = undef,

  $disable_notifications    = false,

  $bgp_to_the_host          = false,
){

  # Tweak performances for network host
  # class { '::oci::tuned': profile => 'network-throughput' }

  if $use_ssl {
    $proto = 'https'
    $messaging_default_port = '5671'
    $messaging_notify_port = '5671'
    $api_port = 443
  } else {
    $proto = 'http'
    $messaging_default_port = '5672'
    $messaging_notify_port = '5672'
    $api_port = 80
  }
  $messaging_default_proto = 'rabbit'
  $messaging_notify_proto = 'rabbit'

  $base_url = "${proto}://${vip_hostname}"
  $keystone_auth_uri  = "${base_url}:${api_port}/identity"
  $keystone_admin_uri = "${base_url}:${api_port}/identity"

  if $self_signed_api_cert {
    $api_endpoint_ca_file = '/etc/ssl/certs/oci-pki-oci-ca-chain.pem'
  }else{
    $api_endpoint_ca_file = ''
  }
  $oci_pki_root_ca_file = '/etc/ssl/certs/oci-pki-oci-ca-chain.pem'

  class { '::oci::puppet_oci_ca_cert':
    self_signed_api_cert => $self_signed_api_cert,
  }

  ###############
  ### Neutron ###
  ###############
  if $vmnet_mtu == 0 {
    $vmnet_mtu_real = 1500
  }else{
    $vmnet_mtu_real = $vmnet_mtu
  }

  if $disable_notifications {
    $neutron_notif_transport_url = ''
  } else {
    $neutron_notif_transport_url = os_transport_url({
                                     'transport' => $messaging_notify_proto,
                                     'hosts'     => fqdn_rotate($all_masters),
                                     'port'      => $messaging_notify_port,
                                     'username'  => 'neutron',
                                     'password'  => $pass_neutron_messaging,
                                   })
  }
  if $openstack_release == 'rocky'{
    class { '::neutron':
      debug                      => true,
      default_transport_url      => os_transport_url({
        'transport' => $messaging_default_proto,
        'hosts'     => fqdn_rotate($all_masters),
        'port'      => $messaging_default_port,
        'username'  => 'neutron',
        'password'  => $pass_neutron_messaging,
      }),
      notification_transport_url => $neutron_notif_transport_url,
      dns_domain                 => $cluster_domain,
      rabbit_use_ssl             => $use_ssl,
      rabbit_ha_queues           => true,
      kombu_ssl_ca_certs         => $oci_pki_root_ca_file,
      amqp_sasl_mechanisms       => 'PLAIN',
      allow_overlapping_ips      => true,
      core_plugin                => 'ml2',
#      service_plugins            => ['router', 'metering', 'firewall', 'qos', 'trunk', 'neutron_lbaas.services.loadbalancer.plugin.LoadBalancerPluginv2'],
      service_plugins            => ['router', 'metering', 'qos', 'trunk', 'firewall_v2'],
      bind_host                  => $machine_ip,
      notification_driver        => 'messagingv2',
      global_physnet_mtu         => $vmnet_mtu_real,
    }
  }else{
    class { '::neutron':
      default_transport_url      => os_transport_url({
        'transport' => $messaging_default_proto,
        'hosts'     => fqdn_rotate($all_masters),
        'port'      => $messaging_default_port,
        'username'  => 'neutron',
        'password'  => $pass_neutron_messaging,
      }),
      notification_transport_url => $neutron_notif_transport_url,
      dns_domain                 => $cluster_domain,
      rabbit_use_ssl             => $use_ssl,
      rabbit_ha_queues           => true,
      kombu_ssl_ca_certs         => $oci_pki_root_ca_file,
      amqp_sasl_mechanisms       => 'PLAIN',
      allow_overlapping_ips      => true,
      core_plugin                => 'ml2',
#      service_plugins            => ['router', 'metering', 'firewall', 'qos', 'trunk', 'neutron_lbaas.services.loadbalancer.plugin.LoadBalancerPluginv2'],
      service_plugins            => ['router', 'metering', 'qos', 'trunk', 'firewall_v2'],
      bind_host                  => $machine_ip,
      notification_driver        => 'messagingv2',
      global_physnet_mtu         => $vmnet_mtu_real,
    }
    class { '::neutron::logging':
      debug => true,
    }
  }
  neutron_config {
    'nova/cafile':         value => $api_endpoint_ca_file;
    'database/connection': value => '';
  }

  class { '::neutron::server::notifications':
    auth_url => $keystone_admin_uri,
    password => $pass_nova_authtoken,
  }

  class { '::neutron::client': }
  class { '::neutron::keystone::authtoken':
    password             => $pass_neutron_authtoken,
    auth_url             => $keystone_admin_uri,
    www_authenticate_uri => $keystone_auth_uri,
    cafile               => $api_endpoint_ca_file,
  }
  class { '::neutron::agents::ml2::ovs':
    local_ip                   => $vmnet_ip,
    tunnel_types               => ['vxlan'],
    bridge_uplinks             => ['eth0'],
    bridge_mappings            => $bridge_mapping_list,
#    extensions                 => 'fwaas_v2',
    extensions                 => '',
    l2_population              => true,
    arp_responder              => false,
    firewall_driver            => 'iptables_hybrid',
    drop_flows_on_start        => false,
    enable_distributed_routing => true,
    manage_vswitch             => false,
  }
  class { '::neutron::agents::l3':
    interface_driver => 'openvswitch',
    debug            => true,
    agent_mode       => 'dvr_snat',
#    extensions       => 'fwaas_v2',
    ha_enabled       => false,
    extensions       => '',
  }
  neutron_l3_agent_config {
    'DEFAULT/external_network_bridge': value => '';
  }

  class { '::neutron::plugins::ml2':
    type_drivers         => ['flat', 'vxlan', 'vlan', ],
    tenant_network_types => ['flat', 'vxlan', 'vlan', ],
    extension_drivers    => 'port_security,qos',
    mechanism_drivers    => 'openvswitch,l2population',
    firewall_driver      => 'iptables_v2',
    flat_networks        => $external_network_list,
    network_vlan_ranges  => $external_network_list,
    vni_ranges           => '1000:9999',
    path_mtu             => $vmnet_mtu,
  }
  class { '::neutron::agents::dhcp':
    interface_driver         => 'openvswitch',
    debug                    => true,
    enable_isolated_metadata => true,
    enable_metadata_network  => true,
  }
#  class { '::neutron::agents::metering':
#    interface_driver => 'openvswitch',
#    debug            => true,
#  }
  class { '::neutron::services::fwaas':
    enabled       => true,
    agent_version => 'v2',
    driver        => 'iptables_v2',
  }
  package { 'l2gw networking':
    name => 'python3-networking-l2gw',
    ensure => installed,
  }

  class { '::neutron::agents::metadata':
    debug             => true,
    shared_secret     => $pass_metadata_proxy_shared_secret,
    metadata_workers  => 2,
    package_ensure    => 'latest',
    metadata_protocol => 'http',
    metadata_host     => 'localhost',
  }

  class { 'haproxy':
    global_options   => {
      'log'     => '/dev/log local0',
      'maxconn' => '256',
      'user'    => 'haproxy',
      'group'   => 'haproxy',
      'daemon'  => '',
      'nbproc'  => '4',
    },
    defaults_options => {
      'mode'    => 'http',
    },
    merge_options => true,
  }->
  haproxy::listen { 'hapstats':
    section_name => 'statshttp',
    bind         => { "${machine_ip}:8088" => 'user root'},
    mode         => 'http',
    options      => {
        'stats' => [ 'uri /', "auth admin:${pass_haproxy_stats}", 'refresh 15', 'admin if TRUE', 'realm Haproxy Statistics', ],
      },
  }

  $haproxy_options_for_metadata_proxy = [
    { 'use_backend' => 'metadatabe' },
  ]
  haproxy::frontend { 'openstackfe':
    mode      => 'http',
    bind      => { "127.0.0.1:8775" => [] },
    options   => $haproxy_options_for_metadata_proxy,
  }
  haproxy::backend { 'metadatabe':
    options => [
       { 'option' => 'forwardfor' },
       { 'mode' => 'http' },
       { 'balance' => 'roundrobin' },
    ],
  }
  if $openstack_release == 'rocky'{
    $metadatabm_options = 'check'
  }else{
    $metadatabm_options = 'check check-ssl ssl verify required ca-file /etc/ssl/certs/oci-pki-oci-ca-chain.pem'
  }
  haproxy::balancermember { 'metadatabm':
    listening_service => 'metadatabe',
    ipaddresses       => $all_masters_ip,
    server_names      => $all_masters,
    ports             => 8775,
    options           => $metadatabm_options
  }

  ############################
  ### OpenVSwitch defaults ###
  ############################
  file { '/etc/default/openvswitch-switch':
    content => '# Managed by Puppet
OVS_CTL_OPTS="--delete-bridges"',
  }
}
