# Install and configure a CEPH MON node for OCI
#
# == Parameters
#
# [*block_devices*]
# List of block devices the volume node can use as LVM backend.
class oci::volume(
  $openstack_release        = undef,
  $machine_hostname         = undef,
  $machine_ip               = undef,
  $first_master             = undef,
  $first_master_ip          = undef,
  $all_masters              = undef,
  $all_masters_ip           = undef,
  $vip_hostname             = undef,
  $vip_ipaddr               = undef,
  $vip_netmask              = undef,
  $self_signed_api_cert     = true,
  $sql_vip_ip               = undef,
  $network_ipaddr           = undef,
  $network_cidr             = undef,
  $use_ssl                  = true,

  $block_devices            = undef,
  $vgname                   = undef,
  $backup_backend           = undef,

  $pass_cinder_db           = undef,
  $pass_cinder_authtoken    = undef,
  $pass_cinder_messaging    = undef,

  $extswift_use_external        = false,
  $extswift_auth_url            = undef,
  $extswift_proxy_url           = undef,
  $extswift_project_name        = undef,
  $extswift_project_domain_name = undef,
  $extswift_user_name           = undef,
  $extswift_user_domain_name    = undef,
  $extswift_password            = undef,

  $lvm_backend_name             = 'LVM_1',
  $availability_zone        = 'nova',
){

  # Generic performances tweak (will default to throughput-performance)
  # class { '::oci::tuned': }

  if $use_ssl {
    $proto = 'https'
    $messaging_default_port = '5671'
    $messaging_notify_port = '5671'
    $api_port = 443
  } else {
    $proto = 'http'
    $messaging_default_port = '5672'
    $messaging_notify_port = '5672'
    $api_port = 80
  }
  $messaging_default_proto = 'rabbit'
  $messaging_notify_proto = 'rabbit'

  $base_url = "${proto}://${vip_hostname}"
  $keystone_auth_uri  = "${base_url}:${api_port}/identity"
  $keystone_admin_uri = "${base_url}:${api_port}/identity"

  if $self_signed_api_cert {
    $api_endpoint_ca_file = '/etc/ssl/certs/oci-pki-oci-ca-chain.pem'
  }else{
    $api_endpoint_ca_file = ''
  }
  $oci_pki_root_ca_file = '/etc/ssl/certs/oci-pki-oci-ca-chain.pem'

  class { '::oci::puppet_oci_ca_cert':
    self_signed_api_cert => $self_signed_api_cert,
  }

  include ::cinder::client
  # Cinder main class (ie: cinder-common config)
  if $openstack_release == 'rocky'{
    class { '::cinder':
      default_transport_url => os_transport_url({
        'transport' => $messaging_default_proto,
        'hosts'     => fqdn_rotate($all_masters),
        'port'      => $messaging_default_port,
        'username'  => 'cinder',
        'password'  => $pass_cinder_messaging,
      }),
      # TODO: Fix hostname !
      database_connection   => "mysql+pymysql://cinder:${pass_cinder_db}@${sql_vip_ip}/cinderdb?charset=utf8",
      rabbit_use_ssl         => $use_ssl,
      rabbit_ha_queues       => true,
      kombu_ssl_ca_certs     => $oci_pki_root_ca_file,
      amqp_sasl_mechanisms   => 'PLAIN',
      debug                 => true,
      storage_availability_zone => $availability_zone,
    }
  }else{
    class { '::cinder':
      default_transport_url => os_transport_url({
        'transport' => $messaging_default_proto,
        'hosts'     => fqdn_rotate($all_masters),
        'port'      => $messaging_default_port,
        'username'  => 'cinder',
        'password'  => $pass_cinder_messaging,
      }),
      # TODO: Fix hostname !
      database_connection   => "mysql+pymysql://cinder:${pass_cinder_db}@${sql_vip_ip}/cinderdb?charset=utf8",
      rabbit_use_ssl         => $use_ssl,
      rabbit_ha_queues       => true,
      kombu_ssl_ca_certs     => $oci_pki_root_ca_file,
      amqp_sasl_mechanisms   => 'PLAIN',
      storage_availability_zone => $availability_zone,
    }
    class { '::cinder::logging':
      debug => true,
    }
  }

  if $extswift_use_external {
    $backup_backend_real = 'cinder.backup.drivers.swift.SwiftBackupDriver'
    $provision_cinder_backup = true
  }else{
    case $backup_backend {
      'ceph': {
        $backup_backend_real = 'cinder.backup.drivers.ceph.CephBackupDriver'
        $provision_cinder_backup = false
      }
      'swift': {
        $backup_backend_real = 'cinder.backup.drivers.swift.SwiftBackupDriver'
        $provision_cinder_backup = true
      }
      'none': {
        $backup_backend_real = ''
        $provision_cinder_backup = false
      }
    }
  }

  cinder_config {
    'nova/cafile':                       value => $api_endpoint_ca_file;
    'service_user/cafile':               value => $api_endpoint_ca_file;
    'DEFAULT/snapshot_clone_size':       value => '200';
    'ssl/ca_file':                       value => $api_endpoint_ca_file;
  }
  if $extswift_use_external == false {
    cinder_config {
      'DEFAULT/backup_driver':             value => $backup_backend_real;
      'DEFAULT/backup_swift_auth_url':     value => $keystone_auth_uri;
      'DEFAULT/backup_swift_ca_cert_file': value => $api_endpoint_ca_file;
    }
  }
  # Configure the authtoken
  class { '::cinder::keystone::authtoken':
    password             => $pass_cinder_authtoken,
    auth_url             => $keystone_admin_uri,
    www_authenticate_uri => $keystone_auth_uri,
    memcached_servers    => $memcached_servers,
    cafile               => $api_endpoint_ca_file,
  }

  # Clear volumes on delete (for data security)
  class { '::cinder::volume':
    volume_clear        => 'zero',
    volume_clear_ionice => '-c3',
  }

  # A cinder-backup service on each volume nodes
  if $provision_cinder_backup {
    class { '::cinder::backup': }
    if $extswift_use_external {
      class { '::cinder::backup::swift':
        backup_swift_url             => $extswift_proxy_url,
        backup_swift_auth_url        => "${extswift_auth_url}",
        backup_swift_container       => 'volumebackups',
        backup_swift_object_size     => 536870912,
#        backup_swift_retry_attempts  => $::os_service_default,
#        backup_swift_retry_backoff   => $::os_service_default,
        backup_swift_user_domain     => $extswift_user_domain_name,
        backup_swift_project_domain  => $extswift_project_domain_name,
        backup_swift_project         => $extswift_project_name,
        backup_compression_algorithm => 'zlib',
      }
      cinder_config {
        'DEFAULT/backup_swift_user':         value => $extswift_user_name;
        'DEFAULT/backup_swift_key':          value => $extswift_password;
        'DEFAULT/backup_swift_auth':         value => 'single_user';
        'DEFAULT/backup_swift_auth_version': value => '3';
      }
    }else{
      cinder_config {
        'DEFAULT/backup_swift_auth': value => 'per_user';
      }
    }
  }

  # Avoids Cinder to lookup for the catalogue
  class { '::cinder::glance':
    glance_api_servers => "${base_url}/image",
  }

  # Configure the LVM backend
  if $openstack_release == 'rocky' or $openstack_release == 'stein'{
    cinder::backend::iscsi { $lvm_backend_name:
      iscsi_ip_address   => $machine_ip,
      volume_group       => $vgname,
      iscsi_protocol     => 'iscsi',
      extra_options      => {
         "${lvm_backend_name}/reserved_percentage"           => { 'value' => '10' },
         "${lvm_backend_name}/volume_clear_size"             => { 'value' => '0' },
         "${lvm_backend_name}/max_over_subscription_ratio"   => { 'value' => '4.0' },
         "${lvm_backend_name}/max_luns_per_storage_group"    => { 'value' => '200' },
         "${lvm_backend_name}/check_max_pool_luns_threshold" => { 'value' => true }
      },
      manage_volume_type => true,
    }
  }else{
    cinder::backend::iscsi { $lvm_backend_name:
      target_ip_address  => $machine_ip,
      volume_group       => $vgname,
      target_protocol    => 'iscsi',
      extra_options      => {
         "${lvm_backend_name}/reserved_percentage"           => { 'value' => '10' },
         "${lvm_backend_name}/volume_clear_size"             => { 'value' => '0' },
         "${lvm_backend_name}/max_over_subscription_ratio"   => { 'value' => '4.0' },
         "${lvm_backend_name}/max_luns_per_storage_group"    => { 'value' => '200' },
         "${lvm_backend_name}/check_max_pool_luns_threshold" => { 'value' => true }
      },
      manage_volume_type => true,
    }
  }

  class { '::cinder::backends':
    enabled_backends => [ $lvm_backend_name ],
  }

}
