# Install tuned and provision a given profile
class oci::tuned (
  $profile = undef,
){
  package { 'tuned': ensure => 'present' }

  if $::service_provider == 'systemd' {
    file { '/etc/systemd/system/tuned.service.d':
      ensure => 'directory',
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
    }
    file { '/etc/systemd/system/tuned.service.d/before.conf':
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => "[Unit]\nBefore=openvswitch-switch.service mysql.service nova-compute.service neutron-openvswitch-agent.service neutron-l3-agent.service haproxy.service\n",
    }
    ~> exec { 'tuned systemctl daemon-reload':
      command     => 'systemctl daemon-reload',
      path        => $::path,
      refreshonly => true,
      before      => Service['tuned'],
    }
  }
  # Enable the service
  service { 'tuned':
    ensure    => 'running',
    enable    => true,
    hasstatus => true,
    require   => Package['tuned'],
  }
  if $profile {
    $profile_real = $profile
  }else{
    $profile_real = 'throughput-performance'
  }

  # Enable the chosen profile
  exec { "tuned-adm profile ${profile_real}":
    unless  => "/bin/true # comment to satisfy puppet syntax requirements
set -e
set -x
if [ \"${profile_real}\" = ''\$(tuned-adm active | sed 's/Current active profile: //') ] ; then
    exit 0
else
    exit 1
fi",
    require => Service['tuned'],
    path    => [ '/sbin', '/bin', '/usr/sbin' ],
    # No need to notify services, tuned-adm restarts them alone
  }

}
