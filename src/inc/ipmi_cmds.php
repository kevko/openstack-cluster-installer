<?php

function perform_ipmitool_cmd($con, $conf, $machine_id, $ipmi_username, $ipmi_password, $ipmi_addr, $ipmi_default_gw, $ipmi_netmask){
    $json["status"] = "success";
    $json["message"] = "Successfuly queried API.";

    $q = "SELECT * FROM machines WHERE id='$machine_id'";
    $r = mysqli_query($con, $q);
    if($r === FALSE){
        $json["status"] = "error";
        $json["message"] = mysqli_error($con);
        return $json;
    }
    $n = mysqli_num_rows($r);
    if($n != 1){
        $json["status"] = "error";
        $json["message"] = "No machine with serial id $machine_id in database.";
        return $json;
    }
    $machine = mysqli_fetch_array($r);

    # This is for HP cloud line
    if($machine["product_name"] == "CL2600 Gen10" || $machine["product_name"] == "CL2800 Gen10"){
        // What's below is proven to work with HP Cloud Line CL2600 and CL2800 machines
        // Set the username
        $cmd = "ipmitool user set name 3 " . escapeshellarg($ipmi_username);
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the password
        $cmd = "ipmitool user set password 3 " . escapeshellarg($ipmi_password);
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Enable the user
        $cmd = "ipmitool user enable 3";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set user privileges for channel 1
        $cmd = "ipmitool user priv 3 4 1";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set user privileges for channel 8
        $cmd = "ipmitool user priv 3 4 8";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set user channel access for channel 1
        $cmd = "ipmitool channel setaccess 1 3 link=on ipmi=on callin=on privilege=4";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set user channel access for channel 8
        $cmd = "ipmitool channel setaccess 8 3 link=on ipmi=on callin=on privilege=4";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Enable Serial On LAN
        $cmd = "ipmitool sol payload enable 1 3";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        $cmd = "ipmitool sol payload enable 8 3";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

// This may eventually be needed if we also want to change the default Administrator password
// ipmitool user set name 4 Administrator
// ipmitool user set password 4 XXXXXX
// ipmitool user enable 4
// ipmitool user priv 4 4 1
// ipmitool user priv 4 4 8
// ipmitool channel setaccess 1 4 link=on ipmi=on callin=on privilege=4
// ipmitool channel setaccess 8 4 link=on ipmi=on callin=on privilege=4

        // Set DHCP Off
        $cmd = "ipmitool lan set 1 ipsrc static";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the IP address
        $cmd = "ipmitool lan set 1 ipaddr " . $ipmi_addr;
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the default GW
        if( isset($ipmi_default_gw)){
            $cmd = "ipmitool lan set 1 defgw ipaddr " . $ipmi_default_gw;
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);
        }

        // Set the netmask
        if( isset($ipmi_netmask)){
            $cmd = "ipmitool lan set 1 netmask " . $ipmi_netmask;
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);
        }

        // Set DHCP Off
        $cmd = "ipmitool lan set 8 ipsrc static";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the IP address
        $cmd = "ipmitool lan set 8 ipaddr " . $ipmi_addr;
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the default GW
        if( isset($ipmi_default_gw)){
            $cmd = "ipmitool lan set 8 defgw ipaddr " . $ipmi_default_gw;
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);
        }

        // Set the netmask
        if( isset($ipmi_netmask)){
            $cmd = "ipmitool lan set 8 netmask " . $ipmi_netmask;
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);
        }

//
// ipmitool lan set 1 ipsrc static
// ipmitool lan set 1 ipaddr <IP-IPMI>
// ipmitool lan set 1 defgw ipaddr <IPMI-GW>
// ipmitool lan set 1 netmask 255.255.255.0
//
// ipmitool lan set 8 ipsrc static
// ipmitool lan set 8 ipaddr <IP-IPMI>
// ipmitool lan set 8 defgw ipaddr <IPMI-GW>
// ipmitool lan set 8 netmask 255.255.255.0
    }else{
        // What's below is proven to work with Dell r440 and r640 machines
        // Set the username
        $cmd = "ipmitool user set name 2 " . escapeshellarg($ipmi_username);
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the password
        $cmd = "ipmitool user set password 2 " . escapeshellarg($ipmi_password);
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set DHCP off
        $cmd = "ipmitool lan set 1 ipsrc static";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the IP address
        $cmd = "ipmitool lan set 1 ipaddr " . $ipmi_addr;
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the default GW
        $cmd = "ipmitool lan set 1 defgw ipaddr " . $ipmi_default_gw;
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set the default GW
        $cmd = "ipmitool lan set 1 netmask " . $ipmi_netmask;
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        // Set some credentials
        $cmd = "ipmitool channel setaccess 1 2 link=on ipmi=on callin=on privilege=4";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        $cmd = "ipmitool sol payload enable 1 2";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        $cmd = "ipmitool user priv 2 4 1";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        $cmd = "/usr/bin/oci-dell-set-default-racadm-config";
        $json["message"] .= "\n$cmd";
        send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);
    }
    if($conf["dellipmi"]["live_image_install_dell_ipmi"] == "1"){
        if($machine["product_name"] == "PowerEdge R640" || $machine["product_name"] == "PowerEdge R440"){
            $cmd = "racadm set BIOS.SysSecurity.AcPwrRcvry On";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set BIOS.SerialCommSettings.RedirAfterBoot Enabled";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set BIOS.SerialCommSettings.ExtSerialConnector Serial1";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set BIOS.SerialCommSettings.ConTermType Vt100Vt220";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.IPMISerial.BaudRate 115200";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.IPMISerial.FlowControl RTS/CTS";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.IPMISerial.HandshakeControl Enabled";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.IPMISOL.BaudRate 115200";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.IPMISOL.Enable Enabled";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.Serial.BaudRate 115200";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.Serial.Enable Enabled";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.SerialRedirection.Enable Enabled";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set iDRAC.WebServer.Enable Enabled";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set BIOS.ProcSettings.ProcVirtualization Enabled";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            $cmd = "racadm set BIOS.ProcSettings.ProcAdjCacheLine Enabled";
            $json["message"] .= "\n$cmd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

        }
    }

    // This sometimes help
    $cmd = "ipmitool lan set 1 arp generate on";
    $json["message"] .= "\n$cmd";
    send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

    $cmd = "ipmitool lan set 1 arp interval 5";
    $json["message"] .= "\n$cmd";
    send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

    // Set the VLAN
    $cmd = "ipmitool lan set 1 vlan id " . $machine["ipmi_vlan"];
    $json["message"] .= "\n$cmd";
    send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

    // Print the lan config
    $cmd = "ipmitool lan print 1";
    $json["message"] .= "\n$cmd";
    $out = send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);
    $json["message"] .= "\n$out";

    return $json;
}

function automatic_ipmi_assign($con, $conf, $machine_id){
    $q = "SELECT * FROM machines WHERE id='$machine_id'";
    $r = mysqli_query($con, $q);
    $n = mysqli_num_rows($r);
    if($n == 1){
        $machine = mysqli_fetch_array($r);
        $machine_dhcp_ip = $machine["dhcp_ip"];
        $machine_dhcp_ip_long = ip2long($machine_dhcp_ip);
        $machine_ipmi_detected_ip = $machine["ipmi_detected_ip"];
        $machine_ipmi_detected_ip_long = ip2long($machine_ipmi_detected_ip);

        # Check if the machine already has its IPMI set on one of the IPMI networks
        $qn = "SELECT * FROM networks WHERE role='ipmi'";
        $rn = mysqli_query($con, $qn);
        $nn = mysqli_num_rows($rn);
        $ipmi_addr_is_ok = "no";
        for($i=0;$i<$nn;$i++){
            $network = mysqli_fetch_array($rn);
            $network_start = ip2long($network["ip"]) + 2;
            $network_end = (pow(2, 32 - $network["cidr"]) - 4) + $network_start;
            if($machine_ipmi_detected_ip_long >= $network_start && $machine_ipmi_detected_ip_long <= $network_end){
                $ipmi_addr_is_ok = "yes";
            }
        }

        if($ipmi_addr_is_ok == "yes"){
            return;
        }

        # See if we're currently updating the ipmi config of the machine,
        # if so, do nothing...
        # Note this is a loosy lock, but that's fine as the report script isn't running so often
        if($machine["ipmi_set_in_progress"] == "no"){
            # Set the ipmi_set_in_progress lock
            $qlock = "UPDATE machines SET ipmi_set_in_progress='yes' WHERE id='$machine_id'";
            mysqli_query($con, $qlock);

            # Attempt to match one of the IPMI networks in the OCI's DB with the machine's DHCP address
            $qn = "SELECT * FROM networks WHERE role='ipmi'";
            $rn = mysqli_query($con, $qn);
            $nn = mysqli_num_rows($rn);
            $ipmi_addr_is_ok = "no";
            $matched_network = "no";
            for($i=0;$i<$nn;$i++){
                $network = mysqli_fetch_array($rn);
                $network_dhcp_start = ip2long($network["ipmi_match_addr"]) + 2;
                $network_dhcp_end = (pow(2, 32 - $network["ipmi_match_cidr"]) - 4) + $network_dhcp_start;
                if($machine_dhcp_ip_long >= $network_dhcp_start && $machine_dhcp_ip_long <= $network_dhcp_end){
                    $matched_network = $network;
                }
            }

            if($matched_network != "no"){
                $net_id = $matched_network["id"];
                if(is_null($matched_network["vlan"])){
                    $net_vlan = "off";
                }else{
                    $net_vlan = $matched_network["vlan"];
                }
                $net_iplong = ip2long($matched_network["ip"]);
                $ipmi_net_def_gateway = long2ip($net_iplong+1);
                switch($matched_network["cidr"]){
                case "32":
                    $ipmi_netmask = "255.255.255.255";
                    break;
                case "31":
                    $ipmi_netmask = "255.255.255.254";
                    break;
                case "30":
                    $ipmi_netmask = "255.255.255.252";
                    break;
                case "29":
                    $ipmi_netmask = "255.255.255.248";
                    break;
                case "28":
                    $ipmi_netmask = "255.255.255.240";
                    break;
                case "27":
                    $ipmi_netmask = "255.255.255.224";
                    break;
                case "26":
                    $ipmi_netmask = "255.255.255.192";
                    break;
                case "25":
                    $ipmi_netmask = "255.255.255.128";
                    break;
                case "24":
                    $ipmi_netmask = "255.255.255.0";
                    break;
                case "23":
                    $ipmi_netmask = "255.255.254.0";
                    break;
                case "22":
                    $ipmi_netmask = "255.255.252.0";
                    break;
               case "21":
                    $ipmi_netmask = "255.255.248.0";
                    break;
               case "20":
                    $ipmi_netmask = "255.255.240.0";
                    break;
               case "19":
                    $ipmi_netmask = "255.255.224.0";
                    break;
               case "18":
                    $ipmi_netmask = "255.255.192.0";
                    break;
               case "17":
                    $ipmi_netmask = "255.255.128.0";
                    break;
                case "16":
                    $ipmi_netmask = "255.255.0.0";
                    break;
                case "15":
                    $ipmi_netmask = "255.254.0.0";
                    break;
                case "14":
                    $ipmi_netmask = "255.252.0.0";
                    break;
                case "13":
                    $ipmi_netmask = "255.248.0.0";
                    break;
                case "12":
                    $ipmi_netmask = "255.240.0.0";
                    break;
                case "11":
                    $ipmi_netmask = "255.224.0.0";
                    break;
                case "10":
                    $ipmi_netmask = "255.192.0.0";
                    break;
                case "9":
                    $ipmi_netmask = "255.128.0.0";
                    break;
                case "8":
                    $ipmi_netmask = "255.0.0.0";
                    break;
                case "7":
                    $ipmi_netmask = "254.0.0.0";
                    break;
                case "6":
                    $ipmi_netmask = "252.0.0.0";
                    break;
                case "5":
                    $ipmi_netmask = "248.0.0.0";
                    break;
                case "4":
                    $ipmi_netmask = "240.0.0.0";
                    break;
                case "3":
                    $ipmi_netmask = "224.0.0.0";
                    break;
                case "2":
                    $ipmi_netmask = "192.0.0.0";
                    break;
                case "1":
                    $ipmi_netmask = "128.0.0.0";
                    break;
                case "0":
                    $ipmi_netmask = "0.0.0.0";
                    break;
                }
                $ret = get_ip_of_machine_on_network($con, $conf, $matched_network["id"], $machine_id);
                if($ret["status"] != "success"){
                    $ret = reserve_ip_address($con, $conf, $net_id, $machine["id"], "ipmi");
                    if($ret["status"] != "success"){
                        print($ret["message"]);
                    }
                    $ret = get_ip_of_machine_on_network($con, $conf, $matched_network["id"], $machine_id);
                }
                if($ret["status"] == "success"){
                    $ipmi_addr = $ret["data"];
                    $hex = base64_encode(openssl_random_pseudo_bytes(9, $crypto_strong));
                    $qmachine = "UPDATE machines SET ipmi_use='yes', ipmi_call_chassis_bootdev='yes', ipmi_addr='$ipmi_addr', ipmi_netmask='$ipmi_netmask', ipmi_default_gw='$ipmi_net_def_gateway', ipmi_vlan='$net_vlan', ipmi_username='". $conf["ipmi"]["automatic_ipmi_username"] ."', ipmi_password='$hex' WHERE id='$machine_id'";
                    mysqli_query($con, $qmachine);
                    $json = perform_ipmitool_cmd($con, $conf, $machine_id, $conf["ipmi"]["automatic_ipmi_username"], $hex, $ipmi_addr, $ipmi_net_def_gateway, $ipmi_netmask);
                }else{
                    $qmachine = "UPDATE machines SET ipmi_use='yes', ipmi_call_chassis_bootdev='yes', ipmi_addr='$ipmi_addr', ipmi_netmask='$ipmi_netmask', ipmi_vlan='$net_vlan', ipmi_username='". $conf["ipmi"]["automatic_ipmi_username"] ."', ipmi_password='$hex' WHERE id='$machine_id'";
                    mysqli_query($con, $qmachine);
                }
            }

            # Release the ipmi_set_in_progress lock
            $qlock = "UPDATE machines SET ipmi_set_in_progress='no' WHERE id='$machine_id'";
            mysqli_query($con, $qlock);
        }
    }
}

function check_if_machines_on_ipmi_network($con, $conf){
    $json["status"] = "success";

    $q = "SELECT serial, id, ipmi_addr, ipmi_detected_ip FROM machines WHERE ipmi_set_in_progress='no'";
    $r = mysqli_query($con, $q);
    $n = mysqli_num_rows($r);
    # For each machine
    $msg = "";
    for($i=0;$i<$n;$i++){
        $machine = mysqli_fetch_array($r);
        $machine_id = $machine["id"];
        $machine_ipmi_addr = $machine["ipmi_addr"];
        $machine_ipmi_detected_ip = $machine["ipmi_detected_ip"];
        $machine_serial = $machine["serial"];

        if($machine_ipmi_addr == $machine_ipmi_detected_ip){
            $machine_ipmi_addr_long = ip2long($machine_ipmi_addr);

            # Check if it's IPMI ip matches one of the IPMI networks
            $qn = "SELECT * FROM networks WHERE role='ipmi'";
            $rn = mysqli_query($con, $qn);
            $nn = mysqli_num_rows($rn);
            $ipmi_addr_is_ok = "no";
            $matched_network = "no";
            for($j=0;$j<$nn;$j++){
                $network = mysqli_fetch_array($rn);
                $network_id = $network["id"];
                $network_start_long = ip2long($network["ip"]) + 2;
                $network_end_long = (pow(2, 32 - $network["cidr"]) - 4) + $network_start_long;
                if($machine_ipmi_addr_long >= $network_start_long && $machine_ipmi_addr_long <= $network_end_long){
                    # We have a match, check if the IPMI IP addr is in the db.
                    $qip = "SELECT * FROM ips WHERE network='$network_id' AND machine='$machine_id' AND usefor='ipmi'";
                    $rip = mysqli_query($con, $qip);
                    $nip = mysqli_num_rows($rip);
                    # If we don't find the IPMI IP in the db, then we are in the
                    # case were we must fix that: record that IP!
                    if($nip == 0){
                        # We first check if this IP exist already in the DB (maybe we have some
                        # IPs that are conflicting?)
                        $qipcheck = "SELECT * FROM ips WHERE ip='$machine_ipmi_addr_long'";
                        $ripcheck = mysqli_query($con, $qipcheck);
                        $nipcheck = mysqli_num_rows($ripcheck);
                        if($nipcheck == 0){
                            # It's now safe to record the IP in the db.
                            $qnip = "INSERT INTO ips (network, ip, type, machine, usefor) VALUES ('$network_id', '$machine_ipmi_addr_long', '4', '$machine_id', 'ipmi')";
                            $rnip = mysqli_query($con, $qnip);
                            $msg .= "Recorded IPMI addr $machine_ipmi_addr for machine with serial $machine_serial.\n";
                        }
                    }
                }
            }
        }
        print("\n\n");
    }
    if($msg == ""){
        $json["message"] = "All checked: done nothing.";
    }else{
        $json["message"] = $msg;
    }
    return $json;
}

?>
