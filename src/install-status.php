<?php

require_once("inc/read_conf.php");
require_once("inc/db.php");
require_once("inc/idna_convert.class.php");
require_once("inc/validate_param.php");
require_once("inc/auth.php");
require_once("inc/ssh.php");

$conf = readmyconf();
$con = connectme($conf);

if(oci_ip_check($con, $conf) === FALSE){
    die("Source IP not in openstack-cluster-installer.conf.");
}

$remote_addr = $_SERVER['REMOTE_ADDR'];

$safe_chassis_serial = safe_serial("chassis-serial");
if($safe_chassis_serial === FALSE){
    $q = "SELECT * FROM machines WHERE dhcp_ip='$remote_addr'";
}else{
    $q = "SELECT * FROM machines WHERE serial='$safe_chassis_serial'";
}

$status = "";
$puppet_status = "";
$update_initial_cluster_setup_var = "no";

$r = mysqli_query($con, $q);
$n = mysqli_num_rows($r);
if($n == 0){
    die();
}else{
    $machine = mysqli_fetch_array($r);
    $id = $machine["id"];
    if( isset($_REQUEST["status"]) ){
        switch($_REQUEST["status"]){
        case "live":
            $status = ", status='live'";
            break;
        case "installing":
            $status = ", status='installing'";
            break;
        case "installed":
            $status = ", status='installed'";
            // Since there can be a host in the between doing SNAT, the REMOTE_ADDR
            // can be wrong, so we fix that by reading the parameter.
            $safe_ipv4 = safe_ipv4("ipaddr");
            if($safe_ipv4 === FALSE){
                error_log("install-status.php: No ipaddr reported after install!");
            }else{
                $remote_addr = $safe_ipv4;
            }
            // Since the machine is booted and installed, we need to sign the puppet cert.
            // We can do it with sudo, as there's a sudoers file installed.
            $output = "";
            $machine_hostname = $machine["hostname"];
            error_log("install-status.php: sudo /usr/bin/puppet cert sign $machine_hostname");
            $cmd = "sudo /usr/bin/puppet cert sign " . $machine_hostname;
            exec($cmd , $output);
            break;
        case "firstboot":
            $status = ", status='firstboot'";
            break;
        case "puppet-running":
            $puppet_status = ", puppet_status='running'";
            break;
        case "puppet-success":
            $puppet_status = ", puppet_status='success'";
            $update_initial_cluster_setup_var = "yes";
            break;
        case "puppet-failure":
            $puppet_status = ", puppet_status='failure'";
            break;
        default:
            $status = ", status='unkown'";
            break;
        }
    }else{
        $status = ", status='unkown'";
    }
    // We keep the machine id...
    if($safe_chassis_serial === FALSE){
        $req = "UPDATE machines SET lastseen=NOW() $puppet_status $status WHERE ipaddr='$remote_addr'";
    }else{
        if($_REQUEST["status"] == "live"){
            $req = "UPDATE machines SET lastseen=NOW() $puppet_status $status, ipaddr='$remote_addr', dhcp_ip='$remote_addr' WHERE serial='$safe_chassis_serial'";
        }else{
            $req = "UPDATE machines SET lastseen=NOW() $puppet_status $status, ipaddr='$remote_addr' WHERE serial='$safe_chassis_serial'";
        }
    }
    $r = mysqli_query($con, $req);
    if($update_initial_cluster_setup_var == "yes" && $safe_chassis_serial !== FALSE){
        $q = "SELECT cluster,role FROM machines WHERE serial='$safe_chassis_serial'";
        $r = mysqli_query($con, $q);
        if($r !== FALSE){
            $n = mysqli_num_rows($r);
            if($n == 1){
                $machine = mysqli_fetch_array($r);
                switch($machine["role"]){
                case "controller":
                    $clusterid = $machine["cluster"];
                    $q = "SELECT hostname FROM machines WHERE cluster='$clusterid' AND role='controller' AND puppet_status NOT LIKE 'success'";
                    $r = mysqli_query($con, $q);
                    if($r !== FALSE){
                        $n = mysqli_num_rows($r);
                        if($n == 0){
                            $q = "UPDATE clusters SET initial_cluster_setup='no'";
                            $r = mysqli_query($con, $q);
                        }
                    }
                    break;
                case "compute":
                    # Since this is a new compute, we must do a nova-manage cell_v2 discover_hosts so that the machine
                    # appears in the hypervisor list.
                    $clusterid = $machine["cluster"];
                    $q = "SELECT ipaddr FROM machines WHERE cluster='$clusterid' AND role='controller' AND puppet_status LIKE 'success' LIMIT 1";
                    $r = mysqli_query($con, $q);
                    if($r !== FALSE){
                        $n = mysqli_num_rows($r);
                        if($n > 0){
                            $controller = mysqli_fetch_array($r);
                            $ip_addr = $controller["ipaddr"];
                            $cmd = "su nova -s /bin/sh -c 'nova-manage cell_v2 discover_hosts'";
                            send_ssh_cmd($conf, $con, $ip_addr, $cmd);
                        }
                    }
                    break;
                default:
                    break;
                }
            }
        }
    }
}

?>
